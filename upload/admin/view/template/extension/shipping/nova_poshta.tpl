<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-nova_poshta" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-nova_poshta" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-total"><span data-toggle="tooltip"
                                                                                      title="<?php echo $nova_poshta_api_key_help; ?>"><?php echo $nova_poshta_api_key_text; ?></span></label>
                        <div class="col-sm-10">
                            <input type="text" name="nova_poshta_api_key"
                                   value="<?php echo $nova_poshta_api_key; ?>"
                                   placeholder="<?php echo $nova_poshta_api_key_text; ?>"
                                   id="input-nova-key" class="form-control"/>

                            <?php if ($error_nova_poshta_api_key) { ?>
                            <div class="alert alert-danger fade in media">
                                <a href="#" class="close" data-dismiss="alert">&times;</a>
                                <strong>Error!</strong> <?php echo $error_nova_poshta_api_key; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-display-insurance"><span data-toggle="tooltip" title="<?php echo $help_nova_poshta_cost; ?>"><?php echo ''; ?></span></label>
                        <div class="col-sm-10">
                            <label class="radio-inline" id="input-display-insurance">
                                <?php if ($nova_poshta_fix_cost) { ?>
                                <input type="radio" name="nova_poshta_fix_cost" value="1" checked="checked" />
                                <?php echo $text_fix; ?>
                                <?php } else { ?>
                                <input type="radio" name="nova_poshta_fix_cost" value="1" />
                                <?php echo $text_fix; ?>
                                <?php } ?>
                            </label>
                            <label class="radio-inline">
                                <?php if (!$nova_poshta_fix_cost) { ?>
                                <input type="radio" name="nova_poshta_fix_cost" value="0" checked="checked" />
                                <?php echo $text_auto; ?>
                                <?php } else { ?>
                                <input type="radio" name="nova_poshta_fix_cost" value="0" />
                                <?php echo $text_auto; ?>
                                <?php } ?>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-cost"><?php echo $entry_cost; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="nova_poshta_cost" value="<?php echo $nova_poshta_cost; ?>" placeholder="<?php echo $entry_cost; ?>" id="input-cost" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="nova_poshta_status" id="input-status" class="form-control">
                                <?php if ($nova_poshta_status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="nova_poshta_sort_order"
                                   value="<?php echo $nova_poshta_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>"
                                   id="input-sort-order" class="form-control"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>