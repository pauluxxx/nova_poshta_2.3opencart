<?php

class ControllerExtensionShippingNovaPoshta extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('extension/shipping/nova_poshta');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('nova_poshta', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');

        $data['nova_poshta_api_key_text'] = $this->language->get('nova_poshta_api_key_text');
        $data['nova_poshta_api_key_help'] = $this->language->get('nova_poshta_api_key_help');

        $data['help_nova_poshta_cost'] = $this->language->get('help_nova_poshta_cost');
        $data['text_fix'] = $this->language->get('text_fix');
        $data['text_auto'] = $this->language->get('text_auto');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');

        $data['entry_cost'] = $this->language->get('entry_cost');

        $data['entry_status'] = $this->language->get('entry_status');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['nova_poshta_api_key'])) {
            $data['error_nova_poshta_api_key'] = $this->error['nova_poshta_api_key'];
        } else {
            $data['error_nova_poshta_api_key'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/shipping/nova_poshta', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('extension/shipping/nova_poshta', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);

        //Custom field
        if (isset($this->request->post['nova_poshta_api_key'])) {
            $data['nova_poshta_api_key'] = $this->request->post['nova_poshta_api_key'];
        } else {
            $data['nova_poshta_api_key'] = $this->config->get('nova_poshta_api_key');
        }
        if (isset($this->request->post['nova_poshta_fix_cost'])) {
            $data['nova_poshta_fix_cost'] = $this->request->post['nova_poshta_fix_cost'];
        } else {
            $data['nova_poshta_fix_cost'] = $this->config->get('nova_poshta_fix_cost');
        }
        if (isset($this->request->post['nova_poshta_cost'])) {
            $data['nova_poshta_cost'] = $this->request->post['nova_poshta_cost'];
        } else {
            $data['nova_poshta_cost'] = $this->config->get('nova_poshta_cost');
        }
        if (isset($this->request->post['nova_poshta_status'])) {
            $data['nova_poshta_status'] = $this->request->post['nova_poshta_status'];
        } else {
            $data['nova_poshta_status'] = $this->config->get('nova_poshta_status');
        }
        if (isset($this->request->post['nova_poshta_sort_order'])) {
            $data['nova_poshta_sort_order'] = $this->request->post['nova_poshta_sort_order'];
        } else {
            $data['nova_poshta_sort_order'] = $this->config->get('nova_poshta_sort_order');
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/shipping/nova_poshta', $data));
    }

    protected function validateApiKey()
    {
        $city_name = 'Ки';
        $api_key = $this->request->post['nova_poshta_api_key'];

        $xml = '<?xml version="1.0" encoding="UTF-8" ?>
<root>
    <modelName>AddressGeneral</modelName>
    <calledMethod>getSettlements</calledMethod>
    <methodProperties>
       <FindByString>' . $city_name . '</FindByString>
        <Warehouse>1</Warehouse>
    </methodProperties>
    <apiKey>' . $api_key . '</apiKey>
</root>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/xml/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $res = simplexml_load_string($response);
        $bool = filter_var($res->success, FILTER_VALIDATE_BOOLEAN);
        if (!$bool) {
            $this->error['nova_poshta_api_key'] = $this->language->get('error_nova_poshta_api_key_auth_fail');
        }
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/shipping/nova_poshta')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if ((utf8_strlen($this->request->post['nova_poshta_api_key']) < 3) || (utf8_strlen($this->request->post['nova_poshta_api_key']) > 64)) {
            $this->error['nova_poshta_api_key'] = $this->language->get('error_nova_poshta_api_key');
        }
        $this->validateApiKey();
        return !$this->error;
    }

}
