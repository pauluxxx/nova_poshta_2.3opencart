<?php

/*
 * English 
 * 
 */

 // Heading
$_['heading_title']    = 'Нова Почта';

// Entry
$_['entry_name']       = 'Нова Почта';
$_['entry_status']     = 'Статус';
$_['entry_cost']     = 'Стоимость';

//Custom Field
$_['entry_status']     = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';

//Text field
$_['text_extension']   = 'Доставка';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';
$_['text_no']        = 'Нет';
$_['text_yes']        = 'Да';
$_['curier_text']      = 'Нужна курьеская доставка';
$_['otdelenie_text']      = 'Показывать отделения';
$_['gmaps_text']      = 'Показывать отделения на карте';
$_['help_nova_poshta_cost']      = 'Стоимость будет расчитана автоматичесик';
$_['text_fix']      = 'Стоимость будет фиксирована';
$_['text_auto']      = 'Стоимость будет расчитана автоматически';


// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_shipping_nova_poshta_api_key'] = 'ApiKey должен быть не мее 3 символов необходим!';
//help
$_['nova_poshta_api_key_help'] = 'Для помощи посетите сайт https://devcenter.novaposhta.ua/';
$_['nova_poshta_api_key_text'] = 'NovaPochtaAPIKey';