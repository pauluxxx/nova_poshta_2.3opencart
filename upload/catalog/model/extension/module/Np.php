<?php

class ModelExtensionModuleNP extends Model
{
    /* Город отправителя */
    /* API ключ */
    private static $api_key = '';

    public function getWarehouses($city_name, $city_ref)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>
               <root>
               	<modelName>AddressGeneral</modelName>
               	<calledMethod>getWarehouses</calledMethod>
               	<methodProperties>
               		<CityName>' . $city_name . '</CityName>
               		<SettlementRef>' . $city_ref . '</SettlementRef>
                    <Language>ru</Language>
               	</methodProperties>
               <apiKey>' . ModelExtensionModuleNP::$api_key . '</apiKey>
               	</root>';
        $xmlr = simplexml_load_string(ModelExtensionModuleNP::send($xml));
        return $xmlr;
    }
    public function editOrderTitle($order_id,$text){
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET shipping_method = '" . $text . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
    }
    static public function send($xml)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.novaposhta.ua/v2.0/xml/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function city($city_name, $limit = 1)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>
	<file>
	<apiKey>' . ModelExtensionModuleNP::$api_key . '</apiKey>
	<modelName>Address</modelName>
	<calledMethod>searchSettlements</calledMethod>
	<methodProperties>
	<CityName>' . $city_name . '</CityName>
	<Limit>' . $limit . '</Limit>
	</methodProperties>
	</file>';
        $xmlr = simplexml_load_string(ModelExtensionModuleNP::send($xml));
        return $xmlr;
    }

    public function city_auto_compl($city_name)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>
<root>
    <modelName>AddressGeneral</modelName>
    <calledMethod>getSettlements</calledMethod>
    <methodProperties>
       <FindByString>' . $city_name . '</FindByString>
        <Warehouse>1</Warehouse>
    </methodProperties>
    <apiKey>' . ModelExtensionModuleNP::$api_key . '</apiKey>
</root>';
        $xmlr = simplexml_load_string(ModelExtensionModuleNP::send($xml));
        return $xmlr;
    }

    public function set_api($api)
    {
        ModelExtensionModuleNP::$api_key = $api;
    }

    public function getCost($city_from_ref = '8d5a980d-391c-11dd-90d9-001a92567626', $city_to_ref = 'db5c88e0-391c-11dd-90d9-001a92567626', $weight = 1,$cost=300)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>
<root>
<modelName>InternetDocument</modelName>
<calledMethod>getDocumentPrice</calledMethod>
    <methodProperties>
    <CitySender>'.$city_from_ref.'</CitySender>
    <CityRecipient>' . $city_to_ref . '</CityRecipient>
    <Weight>' . $weight . '</Weight>
      <Cost>'.$cost.'</Cost>
    <ServiceType>WarehouseWarehouse</ServiceType>
   </methodProperties>
<apiKey>' . ModelExtensionModuleNP::$api_key . '</apiKey>
</root>';
        $xmlr = simplexml_load_string(ModelExtensionModuleNP::send($xml));
        return $xmlr;
    }

}