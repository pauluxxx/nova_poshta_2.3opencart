<?php

class ModelExtensionShippingNovaPoshta extends Model
{
    function getQuote($address)
    {
        $this->load->language('extension/shipping/nova_poshta');
        $quote_data = array();
        $fixed_cost = '';
        if ($this->config->get('nova_poshta_fix_cost')) {
            $fixed_cost = $this->config->get('nova_poshta_cost');
        } else {
            $fixed_cost = $this->getCostFromNP($address);
        }
        $quote_data['nova_poshta'] = array(
            'code' => 'nova_poshta.nova_poshta',
            'title' => $this->language->get('text_description'),
            'cost' => $fixed_cost,
            'tax_class_id' => 0,
            'text' => $this->currency->format($fixed_cost, $this->session->data['currency'])
        );
        $this->load->model('localisation/currency');

        $method_data = array(
            'code' => 'nova_poshta',
            'title' => $this->language->get('text_title'),
            'quote' => $quote_data,
            'sort_order' => $this->config->get('nova_poshta_sort_order'),
            'error' => false
        );

        return $method_data;
    }

    public function addOtdelenie($order_id, $data)
    {
        $this->db->query("insert  into " . DB_PREFIX . "nova_poshta_otdelenie (`otdelenie_id`,`order_id`,`description`) value " . "(null," . $order_id . ",'" . $data . "')");

        $otdelenie_id = $this->db->getLastId();

        return $otdelenie_id;
    }

    public function getCostFromNP($address)
    {
        $this->load->model('extension/module/Np');
        $this->model_extension_module_Np->set_api($this->config->get('shipping_nova_poshta_api_key'));
        $this->load->model('setting/setting');
        $config = $this->model_setting_setting->getSetting('config')['config_address'];

        $res = $this->model_extension_module_Np->city($config);
        if ($res) {
            $city_ref_from = filter_var($res->success, FILTER_VALIDATE_BOOLEAN) ? (string)$res->data->item->Addresses->item->Ref : '';
            $res = $this->model_extension_module_Np->city($address['city']);
            $city_ref_to = filter_var($res->success, FILTER_VALIDATE_BOOLEAN) ? (string)$res->data->item->Addresses->item->Ref : '';
            $products_cost = 0;
            $weight = 0;
            foreach ($this->cart->getProducts() as $product) {
                if ($product['shipping']) {
                    $weight += $product['weight'];
                    $products_cost += $product['price'];
                }
            }
            $res = $this->model_extension_module_Np->getCost($city_ref_from, $city_ref_to, $weight, $products_cost);
            if (filter_var($res->success, FILTER_VALIDATE_BOOLEAN)) {
                return (string)$res->data->item->Cost;
            } else {
                return 60;
            }
        }

    }
}