<div id="np">

    <div class="form-group row">

        <label for="city" class="col-sm-12"><?php echo $city_text ?></label>
        <div class="col-sm-12">
            <div><?php echo $city ?></div>
            <small id="city-change" class="form-text text-muted">
                <a data-toggle="modal" data-target="#city-change-modal">
                    <?php echo $change_city_text ?>
                </a>
            </small>
        </div>

    </div>

    <!-- Modal city -->
    <div class="modal fade" id="city-change-modal" tabindex="-1" role="dialog" aria-labelledby="city-change-modal-label"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="city-change-modal-label"><?php echo $change_city_text ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form action="index.php?route=account/address/edit&address_id="></form>
                        <label for="city" class="col-sm-12"><?php echo $city_text ?></label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="new_city" name="new_city"
                                   placeholder="Введите город" value="<?php echo $city ?>">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal"><?php echo $close_text ?></button>
                    <button type="button" class="btn btn-primary"
                            id="button-city-change"><?php echo $change_text ?></button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal gmap -->
    <div class="modal fade" id="gmap-modal" tabindex="-1" role="dialog" aria-labelledby="gmap--modal-label"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="city-change-modal-label"><?php echo $change_city_text ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    123gmap
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal"><?php echo $close_text ?></button>
                    <button type="button" class="btn btn-primary"
                            id="button-city-change"><?php echo $change_text ?></button>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group row">
        <label for="otdelenie" class="col-sm-12"><?php echo $settelment_text ?></label>
        <div class="col-sm-12">
            <select data-toggle="collapse" class="form-control" name="otdelenie" id="otd"> </select>
            <div class="panel panel-default" id="otd-info">
                <div class="panel-body">

                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $.ajax({
                url: "index.php?route=extension/shipping/nova_poshta/otdelenieAjax",
                type: "post",
                dataType: 'json',
                data: {
                    city: '<?php echo $city ?>'
                },
                beforeSend: function () {
                    $("#otd").prop('disabled', true);
                },
                success: loadNp

            });
        });
    </script>
</div>