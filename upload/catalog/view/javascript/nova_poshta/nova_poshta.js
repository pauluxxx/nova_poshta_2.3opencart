var nova_poshtaDOm;
var t;

loadNp = function (json) {
    $('.alert-dismissible, .text-danger').remove();
    $('.form-group').removeClass('has-error');
    nova_poshtaDOm.prop('disabled', false);
    $("#otd").prop('disabled', false);
    if (json['error']) {
        if (json['error']['error_city_not_found']) {
            $('#shipping_method #otd-info').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['error_city_not_found'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            $("#otd").prop('disabled', true);
            // nova_poshtaDOm.prop('disabled', true);

        }
        if (json['error']['error_no_settelment']) {
            var $option = $('<option value=' + 0 + '>' + json['error']['error_no_settelment'] + '</option>');
            $('#otd').append($option);
            $("#otd").prop('disabled', true);
            // nova_poshtaDOm.prop('disabled', true);
        }
        if (json['error']['20000200068']) {
            var $option = $('<option value=' + 0 + '>' + json['error']['20000200068'] + '</option>');
            $('#otd').append($option);
            $("#otd").prop('disabled', true);
            //Api fail
        }
        $('.text-danger').parent().addClass('has-error');
    } else {
        $.each(json['Descriptions'], function (index, value) {
            var $option = $('<option value=' + index + '>' + value["desk"] + '</option>');
            $('#otd').append($option);
        });
        $('#otd').change(function () {
            var map = new Map();

            function intoRussion(str) {
                switch (str) {
                    case  'Monday':
                        return "Пн"
                    case  'Tuesday':
                        return " Вт"
                    case  'Wednesday':
                        return "Ср"
                    case  'Thursday':
                        return "Чт"
                    case  'Friday':
                        return "Пт"
                    case  'Saturday':
                        return "Сб"
                    case  'Sunday':
                        return "Вс"
                }
            };
            $.each(json['Descriptions'][$('#otd option:selected').val()]['schedule'], function (key, value) {
                map.set(key, value);
            });
            var first = map.values().next().value;
            var firstKey = map.keys().next().value;
            var lastKey = firstKey;
            var $divElement = $('<div id="otd-schedule"></div>');
            var counter = 0;
            today = new Date();
            var todayPointer = false;
            map.forEach(function go(value, key, map) {
                counter++;
                if (today.toLocaleString('en-US', {weekday: 'long'}) == key) {
                    todayPointer = true;
                }
                if (counter == map.size) {
                    if (first != value) {
                        var $sub = $('<div>' + intoRussion(lastKey) + ' : ' + map.get(lastKey) + '</div>');

                        $divElement.append($sub);
                        var $vos;
                        if (map.get(key) == '-') {
                            $vos = $('<div>' + intoRussion(key) + ' : ' + 'Выходой' + '</div>');
                            $vos.addClass('bg-warning');
                        } else {
                            $vos = $('<div>' + intoRussion(key) + ' : ' + map.get(key) + '</div>');
                        }

                        $divElement.append($vos);

                    } else {
                        var $divElement2 = $('<div>' + intoRussion(firstKey) + ' - ' + intoRussion(key) + ' : ' + map.get(key) + '</div>')
                        $divElement.append($divElement2);
                    }
                } else {
                    if (first != value) {
                        var $divElement2 = $('<div>' + intoRussion(firstKey) + ' - ' + intoRussion(lastKey) + ' : ' + map.get(lastKey) + '</div>');
                        $divElement.append($divElement2);
                        firstKey = key;
                        lastKey = firstKey;
                        first = value;

                    } else {
                        lastKey = key;
                    }
                }

            });
            $('#otd-info').html('<div id="otd-tel">Телефон : ' + json['Descriptions'][$('#otd option:selected').val()]['tel'] + "</div>");
            $('#otd-info').append($divElement);
        }).change();
    }
};
saveNP = function () {
    if (nova_poshtaDOm.size()) {
        if (nova_poshtaDOm.prop('checked')) {
            $.ajax({
                url: 'index.php?route=extension/shipping/nova_poshta/save',
                type: 'post',
                data: {otdelenie: $('#otd option:selected').text()},
                dataType: 'json',
                success: function (json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');
                    //nova_poshtaDOm.trigger('change');
                    if (json['error']) {
                        if (json['error']['city']) {
                            $('#shipping_method #otd-info').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['city'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            $("#otd").prop('disabled', true);
                            nova_poshtaDOm.prop('disabled', true);
                            // nova_poshtaDOm.prop('disabled', true);
                        }
                        if (json['error']['no_address']) {
                            $('#shipping_method #otd-info').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['no_address'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            $("#otd").prop('disabled', true);
                            nova_poshtaDOm.prop('disabled', true);
                            // nova_poshtaDOm.prop('disabled', true);
                        }
                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    }
                }
            });
        }
    }
};
renderNp = function () {
    if (nova_poshtaDOm.size()) {
        if (nova_poshtaDOm.prop("checked")) {
            $.ajax({
                url: "index.php?route=extension/shipping/nova_poshta",
                dataType: 'html',
                beforeSend: function () {
                    np = $('#shipping_method').find('#np');
                    np.remove();
                },
                success: function (html) {
                    np = $('#shipping_method').find('#np');
                    np.remove();
                    nova_poshtaDOm.parent().parent().append(html);
                    t = $('#new_city');
                    t.autocomplete({
                            'source': function (request, response) {
                                $.ajax({
                                    url: 'index.php?route=extension/shipping/nova_poshta/autocomplite&q=' + encodeURIComponent(request),
                                    dataType: 'json',
                                    success: function (json) {
                                        response($.map(json, function (item) {
                                            return {
                                                label: item['name'],
                                                value: item['name']
                                            }
                                        }));
                                    }
                                });

                            },
                            'select': function (item) {
                                t.val(item['label']);
                            }
                        }
                    );
                }
            });
        }
    }
};
$(document).ready(function () {
    t = $("#shipping_method");
    var tt = $('#confirm_view');

    tt.bind('DOMSubtreeModified', function () {
        xt = tt.find('button');
        xt.bind('click', function () {
            saveNP();
        })
    });
    t.bind('DOMSubtreeModified', function (event) {
        nova_poshtaDOm = $('#shipping_method').find('input[value="nova_poshta.nova_poshta"]');

        if (nova_poshtaDOm.size()) {
            if (event.target.className === 'qc-step') {
                tr = $('input[type=text]');
                tr.click(function () {
                    renderNp();
                });
                renderNp();
                setTimeout(function () {
                    if (!nova_poshtaDOm.prop('checked')) {
                        $('#np').hide();
                    }
                }, 100);
                $('input[name=shipping_method]').change(function () {
                    if ($(this).val() == 'nova_poshta.nova_poshta') {
                        $('#np').show();
                    } else {
                        $('#np').hide();
                    }
                });
            }
        }
    });
});
$(document).delegate('#button-city-change', 'click', function () {
    $.ajax({
        url: 'index.php?route=extension/shipping/nova_poshta/editCity',
        type: 'post',
        data: $('#new_city'),
        dataType: 'json',
        beforeSend: function () {
            $('#button-city-change').button('loading');
        },
        complete: function () {
            $('#button-city-change').button('reset');
        },
        success: function (json) {
            $('.alert-dismissible, .text-danger').remove();
            $('.form-group').removeClass('has-error');
            if (json['error']) {
                if (json['error']['city']) {
                    $('#shipping_method .modal-footer').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['city'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                if (json['error']['no_address']) {
                    $('#shipping_method .modal-footer').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['no_address'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                $('.text-danger').parent().addClass('has-error');
            } else {
                $('#city-change-modal').modal('hide');
                setTimeout(function () {
                    qc.paymentAddress.updateField('payment_address', json['payment_address']);
                    qc.shippingAddress.updateField('shipping_address', json['shipping_address']);
                    qc.paymentAddressView.render();
                    qc.shippingAddressView.render();
                }, 100);
            }
        },

    });
});
