<?php
/**
 * Created by PhpStorm.
 * User: Павел
 * Date: 27.08.2017
 * Time: 0:16
 */

// Text
$_['text_title'] = 'Новая Почта';
$_['text_description'] = 'Доставка Новой Почтой';
$_['city_text'] = 'Город';
$_['change_city_text'] = 'Изменить город';
$_['settelment_text'] = 'Отделение';
$_['close_text'] = 'Закрыть';
$_['change_text'] = 'Изменить';
$_['$shipment_text'] = 'Доставка';
$_['error_settelment_not_found'] = 'Не найдено отделений!';
$_['error_city_not_found'] = 'Не найдено города в справочнике!';
