<?php

class ControllerExtensionShippingnovaposhta extends Controller
{


    public function index()
    {
        //Load language file
        $this->language->load('extension/shipping/nova_poshta');
        $method_data = array();
        if (isset($this->session->data['shipping_method'])) {
            $data['shipping_method'] = $this->session->data['shipping_method'];
        } else {
            $data['shipping_method'] = array();
        }

        if (isset($this->session->data['shipping_address']['city'])) {
            $data['city'] = $this->session->data['shipping_address']['city'];
        } else {
            $data['city'] = '';
        }
        $data['city_text'] = $this->language->get('city_text');
        $data['change_city_text'] = $this->language->get('change_city_text');
        $data['settelment_text'] = $this->language->get('settelment_text');
        $data['close_text'] = $this->language->get('close_text');
        $data['change_text'] = $this->language->get('change_text');
        $this->document->addScript('catalog/view/javascript/nova_poshta/nova_poshta.js');
        $this->response->setOutput($this->load->view('extension/shipping/nova_poshta', $data));

    }

    public function shippingCorrect()
    {
        $this->load->model('extension/module/Np');
        $order_id = $this->request->get['orderid'];
        $text = $this->request->get['shippingText'];
        $this->model_extension_module_Np->editOrderTitle($order_id, $text);

        $this->load->model('checkout/order');
    /*    $order_info = $this->model_checkout_order->getOrder($order_id);
        $order_info['shipping_method'] = $text;
        $order_info['customer_group_id'] = 1;
        $this->model_checkout_order->editOrder($order_id, $order_info);
*/
        $json['order'] = $this->model_checkout_order->getOrder($order_id);
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function otdelenieAjax()
    {
        $json = array();
        $city_name = '';
        if (isset($this->request->post['city'])) {
            $city_name = $this->request->post['city'];
        } else {
            $city_name = 'Киев';
        }
        $this->load->model('extension/module/Np');
        $this->language->load('extension/shipping/nova_poshta');


        $this->model_extension_module_Np->set_api($this->config->get('shipping_nova_poshta_api_key'));

        $res = $this->model_extension_module_Np->city($city_name);

        if ($res->data->item->TotalCount == '0') {
            $json['error']['error_city_not_found'] = $this->language->get('error_city_not_found');
        } else {
            $city_np = (string)$res->data->item[0]->Addresses->item[0]->MainDescription;
            $ref_sity = (string)$res->data->item[0]->Addresses->item[0]->Ref;
            $resp = $this->model_extension_module_Np->getWarehouses($city_np, $ref_sity);
            $response_data = array();

            foreach ($resp->data->item as $item) {
                array_push($response_data, array('desk' => (string)$item->DescriptionRu, 'schedule' => $item->Schedule, 'tel' => (string)$item->Phone));
            }
            if (count($resp->data->item) == 0) {
                $json['error']['error_no_settelment'] = $this->language->get('error_settelment_not_found');
                $json['Descriptions'] = array('desk' => $this->language->get('error_settelment_not_found'), 'tel' => ' ', 'schedule' => '');
            } else {
                $json['Descriptions'] = $response_data;
            }


        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public
    function save()
    {
        $json = array();
        $this->language->load('extension/shipping/nova_poshta');
        $this->load->model('extension/shipping/nova_poshta');
        $this->load->model('checkout/order');
        $city = $this->language->get('city_text');
        $shipment = $this->language->get('$shipment_text');
        $settelment = '';
        $this->load->model('module/d_quickcheckout');
        if (isset($this->request->post['otdelenie'])) {
            $settelment = $this->request->post['otdelenie'];
            $text_info = $shipment . ' ' . $city . ' ' . $this->session->data['shipping_address']['city'] . ', ' . $settelment;
            $this->session->data['shipping_method']['title'] = $text_info;
        }
        $this->response->setOutput(json_encode($json));
    }

    public
    function editCity()
    {
        $json = array();
        $this->language->load('extension/shipping/nova_poshta');
        if ((utf8_strlen(trim($this->request->post['new_city'])) < 2) || (utf8_strlen(trim($this->request->post['new_city'])) > 128)) {
            $json['error']['city'] = $this->language->get('error_city');
        }
        if ($this->customer->isLogged()) {
            if (isset($this->session->data['shipping_address']['address_id'])) {
                $address_id = $this->session->data['shipping_address']['address_id'];
            }
            if ($address_id) {
                $this->load->model('account/address');
                $address = $this->model_account_address->getAddress($address_id);
                $json['address_id'] = $address_id;
                $address['city'] = $this->request->post['new_city'];
                $this->model_account_address->editAddress($address_id, $address);
                $this->session->data['shipping_address'] = $this->model_account_address->getAddress($address_id);
                //$address_id = $this->model_account_address->getAddress($address_id;
            } else {
                $json['error']['no_address'] = $this->language->get('error_no_address');
            }
        } else {
            $this->session->data['shipping_address']['city'] = $this->request->post['new_city'];
        }
        if ($this->session->data['payment_address']['shipping_address']) {
            $this->session->data['payment_address']['city'] = $this->request->post['new_city'];

        }
        $json['payment_address'] = $this->session->data['payment_address'];
        $json['city'] = $this->session->data['shipping_address']['city'];
        $json['shipping_address'] = $this->session->data['shipping_address'];
        $this->response->setOutput(json_encode($json));

    }

    public
    function autocomplite()
    {
        $json = array();

        if (isset($this->request->get['q'])) {
            $this->load->model('catalog/product');
            $this->load->model('extension/module/Np');
            $this->model_extension_module_Np->set_api($this->config->get('shipping_nova_poshta_api_key'));
            $res = $this->model_extension_module_Np->city($this->request->get['q'], 5);
            $meta_data = array();
            foreach ($res->data->item->Addresses->item as $item) {
                $res = $this->model_extension_module_Np->city_auto_compl($item->MainDescription);
                if ($res->data->item->DescriptionRu != '') {
                    array_push($meta_data, array('name' => strip_tags(html_entity_decode($res->data->item->DescriptionRu, ENT_QUOTES, 'UTF-8'))));
                }
            }
            $json = $meta_data;


        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
